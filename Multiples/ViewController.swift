//
//  ViewController.swift
//  Multiples
//
//  Created by Ryan Breece on 3/23/16.
//  Copyright © 2016 RealFire Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Properties
    var multiple = 0
    var num1 = 0
    var sum = 0
    
    
    @IBOutlet weak var startMessageTxt: UILabel!
    @IBOutlet weak var numberEntry: UITextField!
    @IBOutlet weak var startBtn: UIButton!
    
    @IBOutlet weak var yourCountingTxt: UILabel!
    @IBOutlet weak var mathProblemTxt: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var resetBtn: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func startCounting(sender: UIButton!) {
        if numberEntry != nil && numberEntry != ""{
        multiple = Int(numberEntry.text!)!
        
            startBtn.hidden = true
            numberEntry.hidden = true
            startMessageTxt.hidden = true
            
            yourCountingTxt.hidden = false
            mathProblemTxt.hidden = false
            nextBtn.hidden = false
            resetBtn.hidden = false
            
            numberEntry.resignFirstResponder()
            
        yourCountingTxt.text = "Your Counting by \(multiple)'s "
        mathProblemTxt.text = "\(multiple)+\(num1)=\(sum)"
            
        }
        
    }
    @IBAction func addNext(sender: UIButton!) {
        
        if sum < 100 {
            
            UIView.transitionWithView(mathProblemTxt, duration: 0.8 , options: [.TransitionCrossDissolve], animations: {
                self.mathProblemTxt.text = (rand() % 2 == 0) ? "One" : "Two"
                }, completion: nil)
            
            sum = multiple + sum
            num1 = sum - multiple
            
            
            mathProblemTxt.text = "\(multiple)+\(num1)=\(sum)"
        
        }else {
            resetAll()
            
        }
    }
    @IBAction func resetBtn(sender: AnyObject) {
        resetAll()
    }

    func resetAll() {
        startBtn.hidden = false
        numberEntry.hidden = false
        startMessageTxt.hidden = false
        
        yourCountingTxt.hidden = true
        mathProblemTxt.hidden = true
        nextBtn.hidden = true
        resetBtn.hidden = true
        
        num1 = 0
        sum = 0
        multiple = 0
        
        numberEntry.text="";
    }
}

